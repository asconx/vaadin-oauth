package de.danielhons.vaadinoauth.service;

import com.vaadin.flow.server.VaadinSession;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    public Authentication getCurrentUser(){
        return (Authentication) VaadinSession.getCurrent().getAttribute("user");
    }
}
