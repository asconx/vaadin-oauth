package de.danielhons.vaadinoauth.vaadin;

import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
public class InitListener implements VaadinServiceInitListener {
    @Override
    public void serviceInit(ServiceInitEvent serviceInitEvent) {
        log.info("Publishing Spring Principal to Vaadin Session");
        serviceInitEvent.addRequestHandler((RequestHandler) (session, request, response) -> {
            session.lock();
            session.setAttribute("user", SecurityContextHolder.getContext().getAuthentication());
            session.unlock();
            return false;
        });

    }
}
