package de.danielhons.vaadinoauth.vaadin;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route("forbidden")
public class AccessDenied extends HorizontalLayout {
    public AccessDenied() {
        add(new H1("Forbidden"));
    }
}
