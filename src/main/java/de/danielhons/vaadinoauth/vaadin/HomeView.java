package de.danielhons.vaadinoauth.vaadin;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import de.danielhons.vaadinoauth.service.UserService;

@Route(value = "home", layout = MainView.class)
public class HomeView extends Div {
    public HomeView(UserService userService) {
        add(new H1("Home for " + userService.getCurrentUser().getName()));
    }
}
